package main

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"fmt"
	"encoding/json"
	"context"
)

type Resolver struct{}

func (r *queryResolver) Foo(ctx context.Context, in Input) (*int, error) {
	fmt.Printf("    raw input: %#v\n", in)
	buf, _ := json.Marshal(in.Info)
	fmt.Printf("    as JSON: %s\n", buf)

	var info Info
	json.Unmarshal(buf, &info)

	if info.Items == nil {
		return nil, nil
	}
	
	n := len(info.Items)
	return &n, nil
}

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
