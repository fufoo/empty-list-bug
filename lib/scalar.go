package lib

import (
	"io"
	"fmt"
	"encoding/json"

	"github.com/99designs/gqlgen/graphql"
)

func MarshalJSON(i interface{}) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		bytes, err := json.Marshal(i)
		if err != nil {
			panic("MarshalJSON failed")
		}
		w.Write(bytes)
	})
}

func UnmarshalJSON(v interface{}) (interface{}, error) {
	switch v := v.(type) {
	case map[string]interface{}:
		return v, nil
	case []interface{}:
		return v, nil
	case bool:
		return v, nil
	case string:
		return v, nil
	case json.Number:
		return v, nil
	default:
		return "", fmt.Errorf("%T is not a JSON supported object", v)
	}
}
