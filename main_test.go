package main

import (
	"testing"

	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/stretchr/testify/require"
)

func TestNonEmptyList(t *testing.T) {
	c := client.New(handler.NewDefaultServer(NewExecutableSchema(Config{Resolvers: &Resolver{}})))

	query := `{foo(in:{info: {items: ["hi"]}})}`

	var resp struct {
		Foo *int
	}
	
	c.MustPost(query, &resp)

	require.NotNil(t, resp.Foo)
	require.Equal(t, *resp.Foo, 1)
}

func TestEmptyList(t *testing.T) {
	c := client.New(handler.NewDefaultServer(NewExecutableSchema(Config{Resolvers: &Resolver{}})))

	query := `{foo(in:{info: {items: []}})}`

	var resp struct {
		Foo *int
	}
	
	c.MustPost(query, &resp)

	require.NotNil(t, resp.Foo)
	require.Equal(t, *resp.Foo, 0)
}

func TestMissing(t *testing.T) {
	c := client.New(handler.NewDefaultServer(NewExecutableSchema(Config{Resolvers: &Resolver{}})))

	query := `{foo(in:{info: {items: null}})}`

	var resp struct {
		Foo *int
	}
	
	c.MustPost(query, &resp)

	require.Nil(t, resp.Foo)
}
