# Demonstrate Empty List bug

This repo demonstrates the empty list bug in vektah/gqlparser

As far as I can tell, the bug does not manifest except using something
like a JSON scalar.  In other cases, the argument marshaler will
properly convert a nil list into an empty (zero length) list.

## Rebuild 

```
go run github.com/99designs/gqlgen@latest
```

## Run Test 

```
go test
```

## Apply the Fix

Append this to go.mod:

```
replace github.com/vektah/gqlparser/v2 => github.com/dkolbly/gqlparser/v2 v2.2.1-0.20211216202427-d579b6df640d
```

then:
```
go get bitbucket.org/fufoo/empty-list-bug
```
